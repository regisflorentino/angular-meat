import { CartItem } from "app/restaurant-detail/shop-cart/cart-item.model";
import { MenuItem } from "app/restaurant-detail/menu-item/menu-item.model";
import { Injectable } from "@angular/core";
import { NotificationService } from "app/shared/messages/notification.service";


@Injectable()
export class ShopCartService{

  constructor(private notificationService:NotificationService){}

  items: CartItem[] = []

  clean(){
    this.items = []
  }

  total():number{
    return this.items
      .map(item => item.value())
      .reduce((prev,value) => prev + value, 0)
  }

  addItem(item : MenuItem){
    let foundItem = this.items.find((mItem)=>mItem.menuItem.id === item.id)
    if(foundItem){
      this.incrementaQtd(foundItem);
    }else{
      this.items.push(new CartItem(item))
    }
    this.notificationService.emitirMenssagem(`Você adicionou o item ${item.name} `)
  }

  removeItem(item : CartItem){
    this.items.splice(this.items.indexOf(item),1)
    this.notificationService.emitirMenssagem(`Você removeu o item ${item.menuItem.name}`)
  }
  incrementaQtd(item:CartItem){
    item.quantity = item.quantity+1
  }
  decrementaQtd(item:CartItem){
    console.log(item.quantity)
    item.quantity = item.quantity-1
    console.log(item.quantity)
    if(item.quantity===0){
      this.removeItem(item)
    }
  }

}
