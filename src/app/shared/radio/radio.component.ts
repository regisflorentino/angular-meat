import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { RadioOption } from "app/shared/radio/radio-option.model";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";

@Component({
  selector: 'mt-radio',
  templateUrl: './radio.component.html',
  providers:[
    {provide:NG_VALUE_ACCESSOR,
      useExisting:forwardRef(()=>RadioComponent),
      multi:true}
  ],
  styleUrls: ['./radio.component.css']
})
export class RadioComponent implements OnInit, ControlValueAccessor {
//metodo chamado pelas diretivas quando quer passar um valor para seu componente
writeValue(obj: any): void {
  this.value=obj
}
//funcao chamada sempre q o valor interno do componente mudar
registerOnChange(fn: any): void {
  this.onChange=fn
}
registerOnTouched(fn: any): void {}

setDisabledState(isDisabled: boolean): void {}

//funcao
  onChange:any

  @Input() options: RadioOption[]
  value:any

  setValue(value:any){
    this.value=value
    //chamando o metodo que a interface nos obriga a chamar para avisar o novo valor, avisando as diretivas q usarao os valor do meu componente
    //que o valor mudou
    this.onChange(this.value)
  }

  constructor() { }

  ngOnInit() {
  }

}
