import { Component, OnInit, Input, ContentChild, AfterContentInit } from '@angular/core';
import { NgModel, FormControlName } from "@angular/forms";

@Component({
  selector: 'mt-input-container',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit, AfterContentInit {

  ngAfterContentInit(): void {
    this.input = this.model || this.control
    if(this.input === undefined){
      throw new Error('Esse componente precisa ser usado com diretiva NgModel ou formControlName')
    }
  }

  @Input() input:any
  @Input() label:string
  @Input() menssageError:string
  @Input() showTip : boolean = true


//angular ira injetar essa diretiva
  @ContentChild(NgModel) model: NgModel
  @ContentChild(FormControlName) control: FormControlName

  hasSucess():boolean{
    return this.input.valid && (this.input.dirty || this.input.touched)
  }
  hasError():boolean{
    return this.input.invalid && (this.input.dirty || this.input.touched)
  }

  constructor() { }

  ngOnInit() {
  }

}
