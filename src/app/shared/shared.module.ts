import { NgModule } from "@angular/core";
import { AboutComponent } from "app/about/about.component";
import { InputComponent } from "app/shared/input/input.component";
import { RadioComponent } from "app/shared/radio/radio.component";
import { RatingComponent } from "app/shared/rating/rating.component";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { SnackbarComponent } from './messages/snackbar/snackbar.component';
import { NotificationService } from "app/shared/messages/notification.service";
import { LoginService } from "app/security/login/login.service";
import { LoggedInGuard } from "app/security/loggedin.guard";
import {LeaveOrderGuard} from '../order/leave-order-guard'
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthInterceptor } from "../security/login/auth.interceptor";

@NgModule({
  //declarando os componentes que farao parte desse modulo
  declarations:[InputComponent, RadioComponent, RatingComponent, SnackbarComponent],
  //passando as rotas desse modulo e dependencias do modulo
  imports:[CommonModule, FormsModule, ReactiveFormsModule],
  //falando quem que disponibilizaremos para ser consumido por outros modulos, assim quem utilizar esse
  //modulo n precisaram importar as dependencias desses componetes
  exports:[InputComponent, RadioComponent, RatingComponent,CommonModule, FormsModule, ReactiveFormsModule, SnackbarComponent],
  providers:[NotificationService, LoginService, LoggedInGuard, LeaveOrderGuard,
    {provide: HTTP_INTERCEPTORS, useClass:AuthInterceptor, multi:true}]//registrando nosso interceptos
})
export class SharedModule{

}
