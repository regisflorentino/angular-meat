class Order{

  public endereco:string
  public numero:number
  public complemento:string
  public pagamento:string
  public id?:string
  public itemsCompra:OrderItem[] = []

}
class OrderItem{
  constructor(public quantity:number, public menuId:string){

  }
}
export {Order, OrderItem}
