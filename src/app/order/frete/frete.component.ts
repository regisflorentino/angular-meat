import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'mt-frete',
  templateUrl: './frete.component.html',
  styleUrls: ['./frete.component.css']
})
export class FreteComponent implements OnInit {

  @Input() delivery:number
  @Input() itemValue:number

  constructor() { }

  ngOnInit() {
  }
  total():number{
    return this.delivery + this.itemValue
  }

}
