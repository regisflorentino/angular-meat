import { NgModule } from "@angular/core";
import { OrderService } from "app/order/order.service";
import { RestaurantsService } from "app/restaurants/restaurants.service";
import { ShopCartService } from "app/restaurant-detail/shop-cart/shop-cart.service";

@NgModule({
  //exemplo de core module
  providers:[ShopCartService, RestaurantsService, OrderService]
})
export class CoreModule{

}
