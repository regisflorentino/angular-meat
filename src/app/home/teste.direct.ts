import { ElementRef, Renderer, Directive } from '@angular/core';

@Directive({
    selector: 'tool'
})
export class TesteDirective {

    constructor(private el: ElementRef, private render: Renderer) {
        this.render.setElementAttribute(
            this.el.nativeElement, 'title', 'Good!'
        )
    }

}
