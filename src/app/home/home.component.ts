import { Component, OnInit } from '@angular/core';
import  swal  from 'sweetalert2';

@Component({
  selector: 'mt-home',
  templateUrl: './home.component.html'
  })
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  showMsg() {

    swal('Bem vindo', 'Msg teste', 'success')
  }

}
