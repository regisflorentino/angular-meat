import { CanLoad, Route, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import { LoginService } from "app/security/login/login.service";

@Injectable()
export class LoggedInGuard implements CanLoad, CanActivate{

constructor(private loginService:LoginService){}

//esse metodo se retornado false, impede o carregmento de determinado modulo
  checkAuthentication(path:string):boolean{
    const loggedIn =  this.loginService.isLoggedIn()
    if (!loggedIn) {
      this.loginService.handleLogin(`/${path}`)
    }
    return loggedIn
  }

  canLoad(route: Route): boolean {
    //console.log('load')
    return this.checkAuthentication(route.path)
  }
  //o activateRouterSnaphot é uma foto da rota no momento da chamda do metodo
  //o statesnapshot é a foto da arvore de rotas, podemos acessar os caminhos anteriores
  canActivate(activatedRoute:ActivatedRouteSnapshot, routerState:RouterStateSnapshot):boolean{
  //console.log('activate')
    return this.checkAuthentication(activatedRoute.routeConfig.path)
  }

}
