import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { LoginService } from "app/security/login/login.service";
import {User} from "./user.model";
import { NotificationService } from "app/shared/messages/notification.service";
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'mt-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm : FormGroup
  navigateTo:string

  constructor(private fb: FormBuilder,
     private loginService:LoginService,
     private notificationService: NotificationService,
     private activatedRoute:ActivatedRoute,
   private router:Router) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: this.fb.control('', [Validators.required, Validators.email]),
      password: this.fb.control('', [Validators.required])
    })
    this.navigateTo = this.activatedRoute.snapshot.params['to'] || btoa('/')//se niguem passar uma rota vai pra /
  }
  login(){
    this.loginService
    .login(this.loginForm.value.email, this.loginForm.value.password)
      .subscribe(user => this.notificationService.emitirMenssagem(`Bem vindo, ${user.name}`),
        response =>//httpErrorResponse, se der erro
       this.notificationService.emitirMenssagem(response.error.message),
       //o subscribe pode assumir tres acoes, no sucess, no erros, e na terceira uma acao no fim do observable, que sera
       //essa funcao de callback sem parametros
       ()=>{
          this.router.navigate([atob(this.navigateTo)])//atoab tira o encode q o btoa colocou
       })
  }

}
