import { HomeComponent } from './home/home.component';
import { RestaurantsComponent } from './restaurants/restaurants.component';
import { RestaurantDetailComponent } from './restaurant-detail/restaurant-detail.component';
import { ReviewsComponent } from './restaurant-detail/reviews/reviews.component';
import { MenuComponent } from './restaurant-detail/menu/menu.component';
import { OrderSumaryComponent } from "app/order-sumary/order-sumary.component";
import { NotFoundComponent } from "app/not-found/not-found.component";
import { LoginComponent } from "app/security/login/login.component";
import { LoggedInGuard } from "app/security/loggedin.guard";
export var ROUTES = [
    //path sem caminho, netao o angular ira pro HomeComponent no inicio
    { path: '', component: HomeComponent },
    { path: 'login/:to', component: LoginComponent },
    { path: 'login', component: LoginComponent },
    //{path: 'about', component: AboutComponent},
    //aqui estamos falando q esse modulo sera carregado apenas qd for acessado, é um modulo aparte
    { path: 'restaurants/:id', component: RestaurantDetailComponent,
        children: [
            { path: '', redirectTo: 'menu', pathMatch: 'full' },
            { path: 'menu', component: MenuComponent },
            { path: 'reviews', component: ReviewsComponent }
        ]
    },
    { path: 'order', loadChildren: './order/order.module#OrderModule',
        //o can load diz se vc pode carregar certo modulo
        //o canactivate diz se vc pode entrar nele mesmo apos carregado
        canLoad: [LoggedInGuard], canActivate: [LoggedInGuard] },
    { path: 'order-sumary', component: OrderSumaryComponent },
    { path: 'restaurants', component: RestaurantsComponent },
    { path: 'about', loadChildren: './about/about.module#AboutModule' },
    { path: '**', component: NotFoundComponent }
];
//# sourceMappingURL=app.routes.js.map