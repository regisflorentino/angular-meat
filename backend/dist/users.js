"use strict";
exports.__esModule = true;
var User = (function () {
    function User(email, name, password) {
        this.email = email;
        this.name = name;
        this.password = password;
    }
    User.prototype.verificaEmailEPass = function (another) {
        return another !== undefined && another.email === this.email && another.password === this.password;
    };
    return User;
}());
exports.User = User;
exports.users = {
    "admin@admin": new User('admin@admin', 'admin', 'admin'),
    "admin2@admin": new User('admin2@admin', 'admin2', 'admin2')
};
